#ifndef THERMOMETER_H
#define THERMOMETER_H

#include "owswitch.h"


class AbstractThermometer
{
public:
  virtual void operator()() = 0;
  const float &temperature = _temperature;
  const bool &error = _error;
  virtual void errReset();

protected:
  float _temperature;
  bool _error;
};


struct Calibration
{
  float constant;
  float linCoeff;
  float expCoeff;
  float expInternCoeff;
};


class VoltageThermometer : public AbstractThermometer
{
public:
  static const int &bitDepth;
  static const float &referenceVoltage;
  static int setBitDepth(int);
  static float setReferenceVoltage(float);
private:
  static int _bitDepth;
  static float _referenceVoltage;

public:
  VoltageThermometer();
  VoltageThermometer(int pin);
  void init(int pin);
  const int &pin = _pin;
  Calibration calibration { 0.0f, 0.1f, 1.0f, 1.1f };
  void operator()();
  const float &voltage = _voltage;

private:
  int _pin = -1;
  float _voltage;
};


class DS18B20 : public AbstractThermometer, public OneWireDevice {
public:
  DS18B20(OneWire &bus);
  virtual void operator()();
};


typedef DS18B20 MAX31820;

#endif
