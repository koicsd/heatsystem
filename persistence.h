#ifndef PERSISTENCE
#define PERSISTENCE


class Persistence
{
public:
  static void load();
  static void store();
  static const bool &unsetDeviceBusAddress;

private:
  static bool _unsetDevBusAdr;
};

#endif
