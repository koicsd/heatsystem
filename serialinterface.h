#ifndef SERIALINTERFACE_H
#define SERIALINTERFACE_H

#include "datastructure.h"
#include "thermometer.h"


class SerialInterface
{ 
public:
  static void init();
  static void receive();
  //static void transmit();

  static const bool &masterLostTimeout;

private:
  static bool _masterLostTimeout;
  
  static void limitCharWait(char* rcommand, size_t &n);
  static bool parseCmd(char* rcommand, bool terminated);
  static bool workWithModeSwitch(const String &scommand, bool terminated);
  static bool workWithStatus(const String &scommand, bool terminated);
  static bool workWithThermometers(const String &scommand, bool terminated);
  static bool workWithConfigParams(const String &scommand, bool terminated);
  static bool workWithTempControl(const String &scommand, bool terminated);
  static bool workWithControlOutput(const String &scommand, bool terminated);
  static bool workWithCommand(const String &name, void (*cmdFun)(), const String &scommand, bool terminated);
  template<typename T> static bool workWithQuery(const String &name, const T &variable, void (*print)(const String &prefix, const String &name, const T &value), const String &scommand, bool terminated);
  template<typename T> static bool workWith(const String &name, T &variable, bool (*parse)(const String &sval, T &variable, bool terminated), void (*print)(const String &prefix, const String &name, const T &value), const String &scommand, bool terminated);
  static bool parseFloat(const String &sval, float &variable, bool terminated);
  static bool parseBool(const String &sval, bool &variable, bool terminated);
  static void printFloat(const String &prefix, const String &name, const float &value);
  static void printBool(const String &prefix, const String &name, const bool &value);
  static void printMode(const String &prefix, const String &name, const HeatSysMode &value);
  static void printError(const String &prefix, const String &name, const ErrorCode &value);
  static void printZoneStatus(const String &prefix, const String &name, const ZoneStatus &value);
  static void printThermometer(const String &prefix, const String &name, const AbstractThermometer &value);
  static void printStatus();
  static void printThermometers();
  static void printConfigParams();
  static void printTempControl();
  static void printControlOutput();
};

#endif
