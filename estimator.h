#ifndef ESTIMATOR_H
#define ESTIMATOR_H

#include "datastructure.h"


struct BuildingParams
{

};


class Estimator
{
public:
  static void init();
  static void study();
  static void estimate();
  static const float &radiatorRoomTemp;
  static const float &floorHeatRoomTemp;
  static const bool &error;

private:
  static float _radiatorRoomTemp;
  static float _floorHeatRoomTemp;
  static bool _error;
};

#endif
