#ifndef WALLPANEL_H
#define WALLPANEL_H

#include "datastructure.h"
#include "owswitch.h"
#include "thermometer.h"


class FuchsLCD : public OneWireDevice {
public:
  enum Size{LIN1CH16, LIN2CH16, LIN2CH40, LIN4CH16, LIN4CH20};
  FuchsLCD(OneWire &_bus);
  bool pio[4];
  Size size;
  /*void dispOn();
  void dispOff();
  void lightOn();
  void lightOff();
  void set4pinMode();
  void set8pinMode();
  void setText(char *);
  void clear();
  void operator()();*/
};

struct WallPanel {
  FuchsLCD display;
  DS2413 breakout;
  DS18B20 thermometer;
};

class WallPanelHandler {
  void init(WallPanel&);
  void work(ZoneStatus&, WallPanel&);
};

#endif
