#ifndef OWSWITCH_H
#define OWSWITCH_H

#include "OneWire.h"


class OneWireDevice {
public:
  OneWire &bus;
  byte address[8];

  //OneWireDevice(OneWire &_bus);
};

class OneWireIO : public OneWireDevice {
public:
  OneWireIO(OneWire &_bus);
  //virtual bool digitalRead(int pin) = 0;
  //virtual void digitalWrite(int pin, bool val) = 0;
};

class DS2408 : public OneWireIO {
public:
  DS2408(OneWire &_bus);
  //virtual bool digitalRead(int pin);
  //virtual void digitalWrite(int pin, bool val);
};

class DS2413 : public OneWireIO {
public:
  DS2413(OneWire &_bus);
  //virtual bool digitalRead(int pin);
  //virtual void digitalWrite(int pin, bool val);
};

#endif
