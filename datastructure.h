#ifndef DATASTRUCTURE_H
#define DATASTRUCTURE_H

#include "mytimer.h"


enum HeatSysMode
{
  OFF,
  HEATING,
  MAINTAINANCE
};

struct ZoneStatus
{
  float setpoint;
  float displayValue;
  bool estimated;
  bool heatingNecessary;
};

struct EstimatibleTempValues
{
  float common;
  float difference;
};


enum ErrorCode
{
  OK              = 0,
  UNSAT_ESTIMATION      = 1 << 0,   //  unsatisfying estimation
  MASTER_LOST_TMOUT     = 1 << 1,   //  serial master incoming timeout, possibly head lost
  RAD_ROOM_TEMP_SENS_ERR    = 1 << 2,   //  radiator room thermometer failure
  FLOOR_ROOM_TEMP_SENS_ERR  = 1 << 3,   //  floor-heat room thermometer failure
  WOOD_BOIL_TEMP_SENS_ERR   = 1 << 4,   //  wood boiler thermometer failure
  BUF_TEMP_SENS_ERR     = 1 << 5,   //  buffer thermometer failure
  ESTIMATION_MODE       = 1 << 6,   //  estimation mode active
  SINGLE_TEMP_MEAS_MODE   = 1 << 7,   //  radiator or floor-heat room temperature is considered only (not both)
  WOOD_BOIL_PUMP_DEC_WARN   = 1 << 8,   //  unable to accurately decide whether to operate wood boiler pump or not
  BUF_OR_GAS_BOIL_DEC_ERR   = 1 << 9,   //  unable to decide whether to use buffer or gas boiler -> using gas boiler (wood-based subsystem corrupt)
  HEAT_NEC_DEC_ERR      = 1 << 10,  //  unable to decide whether to heat or not -> heating inactive, whole system corrupt
  UNSET_DEVICE_ADR_ERR    = 1 << 11,  //  devices with unset or invalid bus-address present on one-wire
  FREEZE_EMERGENCY      = 1 << 12,  //  too low temperature values present, danger of freezing
  OVERHEAT_EMERGENCY      = 1 << 13   //  too high temperature values present, danger of steam-explosion
};

#endif
