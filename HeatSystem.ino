#include "main.h"
#include "persistence.h"
#include "tempcontrol.h"
#include "estimator.h"
#include "serialinterface.h"
#include "OneWire.h"


#define ERR_LED PA5
#define BUZZER PA6
#define ACK_BTN PC13
#define OW1_PIN PB10
#define OW2_PIN PA10
//#define WOOD_BOIL_TEMP_SENS_PIN   3
//#define BUF_WAT_TEMP_SENS_PIN   4


OneWire ow1(OW1_PIN);
OneWire ow2(OW2_PIN);

DS2408 Hardware::mainPanel(ow1);
DS2413 Hardware::breakoutPanel(ow1);
WallPanel Hardware::WallPanels::floorHeatZone = {
  .display = FuchsLCD(ow1),
  .breakout = DS2413(ow1),
  .thermometer = DS18B20(ow1)
};
WallPanel Hardware::WallPanels::radiatorZone = {
  .display = FuchsLCD(ow1),
  .breakout = DS2413(ow1),
  .thermometer = DS18B20(ow1)
};
VoltageThermometer Hardware::WaterThermometers::woodBoiler;
VoltageThermometer Hardware::WaterThermometers::buffer;
MAX31820 Hardware::outdoorThermometer(ow2);

AbstractThermometer &Thermometers::radiatorRoom = Hardware::WallPanels::radiatorZone.thermometer;
AbstractThermometer &Thermometers::floorHeatRoom = Hardware::WallPanels::floorHeatZone.thermometer;
AbstractThermometer &Thermometers::woodBoiler = Hardware::WaterThermometers::woodBoiler;
AbstractThermometer &Thermometers::bufferWater = Hardware::WaterThermometers::buffer;
AbstractThermometer &Thermometers::outdoor = Hardware::outdoorThermometer;

HeatSysMode _mode = HeatSysMode::OFF;
const HeatSysMode &Status::mode = _mode;
float Status::radiatorZoneSetpoint = 22.0;
float Status::floorHeatZoneSetpoint = 22.0;
const ZoneStatus &Status::radiatorRoom = TempControl::radiatorRoomStatus;
const ZoneStatus &Status::floorHeatRoom = TempControl::floorHeatRoomStatus;
bool Status::error;
ErrorCode Status::errCode;
const bool &Status::freezeEmergency = TempControl::freezeEmergency;
const bool &Status::overheatEmergency = TempControl::overheatEmergency;

bool ControlOutput::floorHeatPump = false;
bool ControlOutput::woodBoilerPump = false;
bool ControlOutput::bufferPump = false;
bool ControlOutput::gasBoiler = false;

void signError();
void refreshWallTerminal();


void setup()
{
  /*pinMode(FLOOR_HEAT_PUMP_PIN, OUTPUT);
  pinMode(WOOD_BOIL_PUMP_PIN, OUTPUT);
  pinMode(BUFFER_PUMP_PIN, OUTPUT);
  pinMode(GAS_BOIL_PIN, OUTPUT);
  pinMode(ERROR_PIN, OUTPUT);

  Thermometers::radiatorRoom.init(RAD_ROOM_TEMP_SENS_PIN);
  Thermometers::floorHeatRoom.init(FLOOR_ROOM_TEMP_SENS_PIN);
  Thermometers::woodBoiler.init(WOOD_BOIL_TEMP_SENS_PIN);
  Thermometers::bufferWater.init(BUF_WAT_TEMP_SENS_PIN);*/

  Persistence::load();
  Estimator::init();
  SerialInterface::init();
}

void loop()
{
  Estimator::estimate();

  Thermometers::radiatorRoom();
  Thermometers::floorHeatRoom();
  Thermometers::woodBoiler();
  Thermometers::bufferWater();

  SerialInterface::receive();
  TempControl::work();
  Estimator::study();
  signError();
  refreshWallTerminal();

  /*digitalWrite(FLOOR_HEAT_PUMP_PIN, ControlOutput::floorHeatPump);
  digitalWrite(WOOD_BOIL_PUMP_PIN, ControlOutput::woodBoilerPump);
  digitalWrite(BUFFER_PUMP_PIN, ControlOutput::bufferPump);
  digitalWrite(GAS_BOIL_PIN, ControlOutput::gasBoiler);
  digitalWrite(ERROR_PIN, Status::error);*/

  //SerialInterface::transmit();
  Persistence::store();
}

void heatMode(){
  if (_mode == HeatSysMode::MAINTAINANCE) {
    Serial.println("First exit config-mode by 'save-config' or 'discard-config'");
  }
  else {
    _mode = HeatSysMode::HEATING;
    Serial.println("Status.Mode: HEATING");
  }
}

void standby(){
  if (_mode == HeatSysMode::MAINTAINANCE) {
    Serial.println("First exit config-mode by 'save-config' or 'discard-config'");
  }
  else {
    _mode = HeatSysMode::OFF;
    Serial.println("Status.Mode: STANDBY");
  }
}

void config(){
  _mode = HeatSysMode::MAINTAINANCE;
  Serial.println("Status.Mode: CONFIG");
}

void saveConfig(){
  if (_mode != HeatSysMode::MAINTAINANCE){
    Serial.println("First enter config-mode!");
  }
  else {
    //TODO: save to EEPROM
    _mode = HeatSysMode::OFF;
    Serial.println("Status.Mode: STANDBY");
  }
}

void discardConfig(){
  if (_mode != HeatSysMode::MAINTAINANCE){
    Serial.println("First enter config-mode!");
  }
  else {
    //TODO: load from EEPROM
    _mode = HeatSysMode::OFF;
    Serial.println("Status.Mode: STANDBY");
  }
}

void signError()
{
  Status::error = Estimator::error
        ||  SerialInterface::masterLostTimeout
        ||  Thermometers::radiatorRoom.error
        ||  Thermometers::floorHeatRoom.error
        ||  Thermometers::woodBoiler.error
        ||  Thermometers::bufferWater.error
        ||  TempControl::estimationMode
        ||  TempControl::oneRoomTemperatureOnlyMode
        ||  TempControl::heatNecessaryDecisionError
        ||  TempControl::woodBoilerPumpDecisionError
        ||  TempControl::bufferOrGasBoilerDecisionError
        ||  Persistence::unsetDeviceBusAddress
        ||  TempControl::freezeEmergency
        ||  TempControl::overheatEmergency;

  Status::errCode = (ErrorCode) ((Estimator::error ? ErrorCode::UNSAT_ESTIMATION : ErrorCode::OK)
                | (SerialInterface::masterLostTimeout ? ErrorCode::MASTER_LOST_TMOUT : ErrorCode::OK)
                | (Thermometers::radiatorRoom.error ? ErrorCode::RAD_ROOM_TEMP_SENS_ERR : ErrorCode::OK)
                | (Thermometers::floorHeatRoom.error ? ErrorCode::FLOOR_ROOM_TEMP_SENS_ERR : ErrorCode::OK)
                | (Thermometers::woodBoiler.error ? ErrorCode::WOOD_BOIL_TEMP_SENS_ERR : ErrorCode::OK)
                | (Thermometers::bufferWater.error ? ErrorCode::BUF_TEMP_SENS_ERR : ErrorCode::OK)
                | (TempControl::estimationMode ? ErrorCode::ESTIMATION_MODE : ErrorCode::OK)
                | (TempControl::oneRoomTemperatureOnlyMode ? ErrorCode::SINGLE_TEMP_MEAS_MODE : ErrorCode::OK)
                | (TempControl::heatNecessaryDecisionError ? ErrorCode::HEAT_NEC_DEC_ERR : ErrorCode::OK)
                | (TempControl::woodBoilerPumpDecisionError ? ErrorCode::WOOD_BOIL_PUMP_DEC_WARN : ErrorCode::OK)
                | (TempControl::bufferOrGasBoilerDecisionError ? ErrorCode::BUF_OR_GAS_BOIL_DEC_ERR : ErrorCode::OK)
                | (Persistence::unsetDeviceBusAddress ? ErrorCode::UNSET_DEVICE_ADR_ERR : ErrorCode::OK)
                | (TempControl::freezeEmergency ? ErrorCode::FREEZE_EMERGENCY : ErrorCode::OK)
                | (TempControl::overheatEmergency ? ErrorCode::OVERHEAT_EMERGENCY : ErrorCode::OK));
}

void refreshWallTerminal()
{

}
