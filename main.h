#ifndef MAIN_H
#define MAIN_H

#include "datastructure.h"
#include "thermometer.h"
#include "owswitch.h"
#include "wallpanel.h"


struct Hardware {
  static DS2408 mainPanel;
  static DS2413 breakoutPanel;
  struct WallPanels {
    static WallPanel floorHeatZone;
    static WallPanel radiatorZone;
  };
  struct WaterThermometers {
    static VoltageThermometer woodBoiler;
    static VoltageThermometer buffer;
  };
  static MAX31820 outdoorThermometer;
};


struct Thermometers
{
  static AbstractThermometer &radiatorRoom;
  static AbstractThermometer &floorHeatRoom;
  static AbstractThermometer &woodBoiler;
  static AbstractThermometer &bufferWater;
  static AbstractThermometer &outdoor;
};


struct Status
{
  static const HeatSysMode &mode;
  static float radiatorZoneSetpoint;
  static float floorHeatZoneSetpoint;
  static const ZoneStatus &radiatorRoom;
  static const ZoneStatus &floorHeatRoom;
  static bool error;
  static ErrorCode errCode;
  static const bool &freezeEmergency;
  static const bool &overheatEmergency;
};


struct ControlOutput
{
  static bool floorHeatPump;
  static bool woodBoilerPump;
  static bool bufferPump;
  static bool gasBoiler;
};


void heatMode();
void standby();
void config();
void saveConfig();
void discardConfig();

#endif
