#include "estimator.h"
#include "main.h"


float Estimator::_radiatorRoomTemp;
float Estimator::_floorHeatRoomTemp;
bool Estimator::_error;
const float &Estimator::radiatorRoomTemp = Estimator::_radiatorRoomTemp;
const float &Estimator::floorHeatRoomTemp = Estimator::_floorHeatRoomTemp;
const bool &Estimator::error = Estimator::_error;

void Estimator::init()
{

}

void Estimator::study()
{

}

void Estimator::estimate()
{

}
