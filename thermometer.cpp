#include "thermometer.h"

#include "arduino.h"
#include "math.h"


void AbstractThermometer::errReset()
{
  _error = false;
}


int VoltageThermometer::_bitDepth = 10;
float VoltageThermometer::_referenceVoltage = 5.0;
const int &VoltageThermometer::bitDepth = VoltageThermometer::_bitDepth;
const float &VoltageThermometer::referenceVoltage = VoltageThermometer::referenceVoltage;

int VoltageThermometer::setBitDepth(int value)
{
  if (value >= 1 && value <= 32)
  {
    _bitDepth = value;
  }
  return _bitDepth;
}

float VoltageThermometer::setReferenceVoltage(float value)
{
  if (value >= 0 && value <= 5)
  {
    _referenceVoltage = value;
  }
  return _referenceVoltage;
}

VoltageThermometer::VoltageThermometer()
{

}

VoltageThermometer::VoltageThermometer(int pin)
{
  init(pin);
}

void VoltageThermometer::init(int pin)
{
  _pin = pin;
}

void VoltageThermometer::operator()()
{
  if (_pin < 0)
  {
    _error = true;
    return;
  }
  //_voltage = analogRead(_pin) * _referenceVoltage / pow(2, _bitDepth);
  //_temperature = calibration.constant + calibration.linCoeff * _voltage + calibration.expCoeff * exp(calibration.expInternCoeff * _voltage);
}

DS18B20::DS18B20(OneWire &_bus) : OneWireDevice{.bus = _bus} {}

void DS18B20::operator()() {

}
