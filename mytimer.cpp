#include "mytimer.h"
#include "arduino.h"


StepTimer::StepTimer()
{
  _lastSysTime = millis();
}

StepTimer::StepTimer(unsigned int timeout) : timeout(timeout)
{
  _lastSysTime = millis();
}

void StepTimer::operator()()
{
  unsigned long _curSysTime = millis();
  if (!freeze)
  {
    if (!enable)
    {
      _active = false;
      _timeEllapsed = 0;
      _timeLeft = timeout;
      _ready = false;
    }
    else
    {
      _timeEllapsed += (unsigned int)(_curSysTime - _lastSysTime);
      if (_timeEllapsed < timeout)
      {
        _timeLeft = timeout - _timeEllapsed;
        _active = true;
        _ready = false;
      }
      else
      {
        _timeLeft = 0;
        _active = false;
        _ready = true;
      }
    }
  }
  _lastSysTime = _curSysTime;
}

void StepTimer::operator()(bool enable, bool &ready)
{
  this->enable = enable;
  (*this)();
  ready = this->ready;
}

void StepTimer::operator()(bool enable, bool &active, bool &ready)
{
  this->enable = enable;
  (*this)();
  active = this->active;
  ready = this->ready;
}

void StepTimer::operator()(bool enable, bool &active, bool &timeEllapsed, bool &timeLeft, bool &ready)
{
  this->enable = enable;
  (*this)();
  active = this->active;
  timeEllapsed = this->timeEllapsed;
  timeLeft = this->timeLeft;
  ready = this->ready;
}

void StepTimer::operator()(bool enable, bool freeze, bool &active, bool &ready)
{
  this->enable = enable;
  this->freeze = freeze;
  (*this)();
  active = this->active;
  ready = this->ready;
}

void StepTimer::operator()(bool enable, bool freeze, bool &active, bool &timeEllapsed, bool &timeLeft, bool &ready)
{
  this->enable = enable;
  this->freeze = freeze;
  (*this)();
  active = this->active;
  timeEllapsed = this->timeEllapsed;
  timeLeft = this->timeLeft;
  ready = this->ready;
}


CallbackTimer::CallbackTimer(MyTimeoutHandler target)
{
  _target = target;
  _mode = MyTimerMode::STOPPED;
}

void CallbackTimer::operator()()
{
  if (_mode != MyTimerMode::STOPPED)
  {
    unsigned long _curSysTime = millis();
    unsigned _timeEllapsed = (unsigned)(_curSysTime - _lastSysTime);
    if (_timeLeft > _timeEllapsed)
    {
      _timeLeft -= _timeEllapsed;
    }
    else
    {
      _timeLeft = 0;
      _target(this);
      if (_mode == MyTimerMode::REPEATING)
      {
        _timeLeft = _timeout;
      }
      else
      {
        finish();
      }
    }
  }
}

unsigned int CallbackTimer::single(unsigned int timeout)
{
  _mode = MyTimerMode::SINGLE;
  _timeout = timeout;
  _timeLeft = _timeout;
  return _timeout;
}

unsigned int CallbackTimer::repeat(unsigned int timeout)
{
  _mode = MyTimerMode::REPEATING;
  _timeout = timeout;
  _timeLeft = _timeout;
  return _timeout;
}

void CallbackTimer::finish()
{
  _mode = MyTimerMode::STOPPED;
  _timeout = 0;
  _timeLeft = 0;
}


TimedSetpoint::TimedSetpoint(float defaultVal)
{
  _active = false;
  _timeout = false;
  _defaultVal = defaultVal;
  _lastSysTime = millis();
  _timeLeft = 0;
}

void TimedSetpoint::operator()()
{
  //assert _active == (_timeLeft > 0);
  if (_active)
  {
    unsigned long _curSysTime = millis();
    unsigned _timeEllapsed = (unsigned)(_curSysTime - _lastSysTime);
    if (_timeLeft > _timeEllapsed)
    {
      _timeLeft -= _timeEllapsed;
    }
    else
    {
      _timeLeft = 0;
      _active = false;
      _timeout = true;
    }
  }
}

float TimedSetpoint::set(float value, unsigned timeout)
{
  _value = value;
  _timeLeft = timeout;
  _timeout = false;
  _active = true;
  return _value;
}

float TimedSetpoint::value()
{
  //assert _active == (_timeLeft > 0);
  return _active ? _value : _defaultVal;
}

void TimedSetpoint::reset()
{
  _active = false;
  _timeLeft = 0;
  _timeout = false;
}
