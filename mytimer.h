#ifndef MYTIMER_H
#define MYTIMER_H


class StepTimer
{
public:
  StepTimer();
  StepTimer(unsigned int timeout);
  unsigned int timeout;
  bool enable;
  bool freeze;
  const bool &active = _active;
  void operator()();
  void operator()(bool enable, bool &ready);
  void operator()(bool enable, bool &active, bool &ready);
  void operator()(bool enable, bool &active, bool &timeEllapsed, bool &timeLeft, bool &ready);
  void operator()(bool enable, bool freeze, bool &active, bool &ready);
  void operator()(bool enable, bool freeze, bool &active, bool &timeEllapsed, bool &timeLeft, bool &ready);
  const unsigned int &timeEllapsed = _timeEllapsed;
  const unsigned int &timeLeft = _timeLeft;
  const bool &ready = _ready;

private:
  bool _active;
  unsigned long _lastSysTime;
  unsigned int _timeEllapsed;
  unsigned int _timeLeft;
  bool _ready;
};


enum MyTimerMode
{
  STOPPED,
  SINGLE,
  REPEATING
};

class CallbackTimer;
typedef void(*MyTimeoutHandler)(CallbackTimer*);

class CallbackTimer
{
public:
  CallbackTimer(MyTimeoutHandler target);
  void operator()();
  unsigned int single(unsigned int timeout);
  unsigned int repeat(unsigned int timeout);
  const MyTimerMode &mode = _mode;
  const unsigned int &timeout = _timeout;
  const unsigned int &timeLeft = _timeLeft;
  void finish();

private:
  MyTimeoutHandler _target;
  MyTimerMode _mode;
  unsigned int _timeout;
  unsigned int _timeLeft;
  unsigned long _lastSysTime;
};


class TimedSetpoint
{
public:
  TimedSetpoint(float defaultVal = 20.5);
  void operator()();
  float set(float value, unsigned timeout);
  void reset();
  const bool &active = _active;
  float value();
  const unsigned int &timeLeft = _timeLeft;
  const bool &timeout = _timeout;
  float defaultVal;

private:
  bool _active = false;
  float _defaultVal;
  float _value;
  unsigned _timeLeft = 0;
  unsigned long _lastSysTime;
  bool _timeout = false;
};

#endif
