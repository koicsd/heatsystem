#include "math.h"

#include "tempcontrol.h"
#include "main.h"
#include "estimator.h"


const float ConfigParams::FREEZE_ALARM_TEMP = 5;
const float ConfigParams::FREEZE_ALARM_DISA_TEMP = 10;
const float ConfigParams::OVERHEAT_ALARM_TEMP = 90;
const float ConfigParams::OVERHEAT_LOCK_OPEN_TEMP = 50;
float ConfigParams::delthaTemp = 0.5;
float ConfigParams::minBufWaterTemp = 35;

ZoneStatus TempControl::_radiatorRoomStatus;
ZoneStatus TempControl::_floorHeatRoomStatus;
bool TempControl::_normalMode;
bool TempControl::_estimationMode;
bool TempControl::_oneRoomTemperatureOnlyMode;
bool TempControl::_heatNecessaryDecisionError;
bool TempControl::_woodBoilerPumpDecisionError;
bool TempControl::_bufferOrGasBoilerDecisionError;
bool TempControl::_freezeEmergency;
bool TempControl::_overheatEmergency;
bool TempControl::_roomTempSensOk;
bool TempControl::_radRoomEstimation;
bool TempControl::_floorRoomEstimation;
bool TempControl::_radRoomOnly;
bool TempControl::_floorRoomOnly;
bool TempControl::_cannotDecideToHeat;
bool TempControl::_watTempSensOk;
bool TempControl::_woodBoilTempOny;
bool TempControl::_bufWatTempOnly;
bool TempControl::_noWatTempData;
const ZoneStatus &TempControl::radiatorRoomStatus = TempControl::_radiatorRoomStatus;
const ZoneStatus &TempControl::floorHeatRoomStatus = TempControl::_floorHeatRoomStatus;
const bool &TempControl::normalMode = TempControl::_normalMode;
const bool &TempControl::estimationMode = TempControl::_estimationMode;
const bool &TempControl::oneRoomTemperatureOnlyMode = TempControl::_oneRoomTemperatureOnlyMode;
const bool &TempControl::heatNecessaryDecisionError = TempControl::_heatNecessaryDecisionError;
const bool &TempControl::woodBoilerPumpDecisionError = TempControl::_woodBoilerPumpDecisionError;
const bool &TempControl::bufferOrGasBoilerDecisionError = TempControl::_bufferOrGasBoilerDecisionError;
const bool &TempControl::freezeEmergency = TempControl::_freezeEmergency;
const bool &TempControl::overheatEmergency = TempControl::_overheatEmergency;


void TempControl::work()
{
  checkError();
  cpyTempVal();
  decideHeatNec();
  prepareOutput();
  handleEmergency();
  signError();
}

void TempControl::checkError()
{
  _roomTempSensOk = !Thermometers::radiatorRoom.error && !Thermometers::floorHeatRoom.error;
  _radRoomEstimation = Thermometers::radiatorRoom.error && !Estimator::error;
  _floorRoomEstimation = Thermometers::floorHeatRoom.error && !Estimator::error;
  _radRoomOnly = Thermometers::floorHeatRoom.error && Estimator::error && !Thermometers::radiatorRoom.error;
  _floorRoomOnly = Thermometers::radiatorRoom.error && Estimator::error && !Thermometers::floorHeatRoom.error;
  _cannotDecideToHeat = Thermometers::radiatorRoom.error && Thermometers::floorHeatRoom.error && Estimator::error;

  _watTempSensOk = !Thermometers::woodBoiler.error && !Thermometers::bufferWater.error;
  _woodBoilTempOny = Thermometers::bufferWater.error && !Thermometers::woodBoiler.error;
  _bufWatTempOnly = Thermometers::woodBoiler.error && !Thermometers::bufferWater.error;
  _noWatTempData = Thermometers::woodBoiler.error && Thermometers::bufferWater.error;
}

void TempControl::cpyTempVal()
{
  _radiatorRoomStatus.setpoint = Status::radiatorZoneSetpoint;
  _radiatorRoomStatus.displayValue =  !Thermometers::radiatorRoom.error ?
                    Thermometers::radiatorRoom.temperature :
                    !Estimator::error ?
                    Estimator::radiatorRoomTemp :
                    NAN;
  _radiatorRoomStatus.estimated = _radRoomEstimation;

  _floorHeatRoomStatus.setpoint = Status::floorHeatZoneSetpoint;
  _floorHeatRoomStatus.displayValue = !Thermometers::floorHeatRoom.error ?
                    Thermometers::floorHeatRoom.temperature :
                    !Estimator::error ?
                    Estimator::floorHeatRoomTemp :
                    NAN;
  _floorHeatRoomStatus.estimated = _floorRoomEstimation;
}

void TempControl::decideHeatNec()
{
  // deciding if heating is necessary
  switch (Status::mode)
  {
  case HeatSysMode::HEATING:
    // keeping around setpoint
    if (Status::radiatorRoom.setpoint - Status::radiatorRoom.displayValue > ConfigParams::delthaTemp)
    {
      _radiatorRoomStatus.heatingNecessary = true;
    }
    else if (Status::radiatorRoom.displayValue - Status::radiatorRoom.setpoint > ConfigParams::delthaTemp)
    {
      _radiatorRoomStatus.heatingNecessary = false;
    }
    if (Status::floorHeatRoom.setpoint - Status::floorHeatRoom.displayValue > ConfigParams::delthaTemp)
    {
      _floorHeatRoomStatus.heatingNecessary = true;
    }
    else if (Status::floorHeatRoom.displayValue - Status::floorHeatRoom.setpoint > ConfigParams::delthaTemp)
    {
      _floorHeatRoomStatus.heatingNecessary = false;
    }

    if (_floorRoomOnly) // rad. room temp. unknown -> use floor-heat thermometer
    {
      _radiatorRoomStatus.heatingNecessary = Status::floorHeatRoom.heatingNecessary;
    }
    else if (_radRoomOnly)  // floor-heat room temp. unknown -> use rad. thermometer
    {
      _floorHeatRoomStatus.heatingNecessary = Status::radiatorRoom.heatingNecessary;
    }
    else if (_cannotDecideToHeat) // no clue about any room-temperature -> OFF
    {
      _radiatorRoomStatus.heatingNecessary = false;
      _floorHeatRoomStatus.heatingNecessary = false;
    }
    break;
  default:  // OFF or maintenance
    _radiatorRoomStatus.heatingNecessary = false;
    _floorHeatRoomStatus.heatingNecessary = false;
    break;
  }
}

void TempControl::prepareOutput()
{
  switch (Status::mode)
  {
  case HeatSysMode::HEATING:
    ControlOutput::floorHeatPump = Status::floorHeatRoom.heatingNecessary && Status::radiatorRoom.heatingNecessary;
    ControlOutput::woodBoilerPump = (!Thermometers::woodBoiler.error && !Thermometers::bufferWater.error) ?
      (Thermometers::woodBoiler.temperature > Thermometers::bufferWater.temperature)
      : false;
    ControlOutput::bufferPump = Status::radiatorRoom.heatingNecessary && ((!Thermometers::bufferWater.error) ?
      (Thermometers::bufferWater.temperature >= ConfigParams::minBufWaterTemp)
      : false);
    ControlOutput::gasBoiler = Status::radiatorRoom.heatingNecessary && ((!Thermometers::bufferWater.error) ?
      (Thermometers::bufferWater.temperature < ConfigParams::minBufWaterTemp)
      : true);
    break;
  case HeatSysMode::OFF:
    ControlOutput::floorHeatPump = false;
    ControlOutput::woodBoilerPump = (!Thermometers::woodBoiler.error && !Thermometers::bufferWater.error) ?
      (Thermometers::woodBoiler.temperature > Thermometers::bufferWater.temperature)
      : false;
    ControlOutput::bufferPump = false;
    ControlOutput::gasBoiler = false;
    break;
  }
}

void TempControl::handleEmergency()
{
  if (Thermometers::woodBoiler.temperature > ConfigParams::OVERHEAT_ALARM_TEMP || Thermometers::bufferWater.temperature > ConfigParams::OVERHEAT_ALARM_TEMP)
  {
    
    _overheatEmergency = true;
  }

  if (Thermometers::radiatorRoom.temperature < ConfigParams::FREEZE_ALARM_TEMP
    || Thermometers::floorHeatRoom.temperature < ConfigParams::FREEZE_ALARM_TEMP
    || Thermometers::woodBoiler.temperature < ConfigParams::FREEZE_ALARM_TEMP
    || Thermometers::bufferWater.temperature < ConfigParams::FREEZE_ALARM_TEMP)
  {
    _freezeEmergency = true;
  }
  else if (Thermometers::radiatorRoom.temperature > ConfigParams::FREEZE_ALARM_DISA_TEMP
    && Thermometers::floorHeatRoom.temperature > ConfigParams::FREEZE_ALARM_DISA_TEMP
    && Thermometers::woodBoiler.temperature > ConfigParams::FREEZE_ALARM_DISA_TEMP
    && Thermometers::bufferWater.temperature > ConfigParams::FREEZE_ALARM_DISA_TEMP)
  {
    _freezeEmergency = false;
  }

  if (_overheatEmergency)
  {
    // ???
  }
  else if (_freezeEmergency)
  {
    // ???
  }
}

void TempControl::signError()
{
  // ???
}
