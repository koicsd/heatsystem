#ifndef TEMPCONTROL
#define TEMPCONTROL

#include "datastructure.h"


struct ConfigParams
{
  static const float FREEZE_ALARM_TEMP;
  static const float FREEZE_ALARM_DISA_TEMP;
  static const float OVERHEAT_ALARM_TEMP;
  static const float OVERHEAT_LOCK_OPEN_TEMP;
  static float delthaTemp;
  static float minBufWaterTemp;
};


class TempControl
{
public:
  static void work();
  static const ZoneStatus &radiatorRoomStatus;
  static const ZoneStatus &floorHeatRoomStatus;
  static const bool &normalMode;
  static const bool &estimationMode;
  static const bool &oneRoomTemperatureOnlyMode;
  static const bool &heatNecessaryDecisionError;
  static const bool &woodBoilerPumpDecisionError;
  static const bool &bufferOrGasBoilerDecisionError;
  static const bool &freezeEmergency;
  static const bool &overheatEmergency;

private:
  static ZoneStatus _radiatorRoomStatus;
  static ZoneStatus _floorHeatRoomStatus;
  static bool _normalMode;
  static bool _estimationMode;
  static bool _oneRoomTemperatureOnlyMode;
  static bool _heatNecessaryDecisionError;
  static bool _woodBoilerPumpDecisionError;
  static bool _bufferOrGasBoilerDecisionError;
  static bool _freezeEmergency;
  static bool _overheatEmergency;
  static bool _roomTempSensOk;
  static bool _radRoomEstimation;
  static bool _floorRoomEstimation;
  static bool _radRoomOnly;
  static bool _floorRoomOnly;
  static bool _cannotDecideToHeat;
  static bool _watTempSensOk;
  static bool _woodBoilTempOny;
  static bool _bufWatTempOnly;
  static bool _noWatTempData;
  static void checkError();
  static void cpyTempVal();
  static void decideHeatNec();
  static void prepareOutput();
  static void handleEmergency();
  static void signError();
};

#endif
