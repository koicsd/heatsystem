#include "arduino.h"
#include "serialinterface.h"
#include "main.h"
#include "tempcontrol.h"
#include "estimator.h"
#include "persistence.h"


#define MAX_CMD_LEN 60
#define MAX_MILLIS_WAIT_CHAR 5000

bool SerialInterface::_masterLostTimeout;
const bool &SerialInterface::masterLostTimeout = SerialInterface::_masterLostTimeout;
//String SerialInterface::_text;


void SerialInterface::init()
{
  Serial.begin(9600);
  while (!Serial);
}


void SerialInterface::receive()
{
  static char rcommand[MAX_CMD_LEN+1];
  static size_t n = 0;
  if (size_t ser_avl = Serial.available()) {
    size_t mem_avl = MAX_CMD_LEN - n;
    size_t read = mem_avl < ser_avl ?
                    Serial.readBytesUntil('\n', rcommand + n, mem_avl):
                    Serial.readBytesUntil('\n', rcommand + n, ser_avl);
    bool terminated = read < ser_avl;
    n += read;
    if (parseCmd(rcommand, terminated) || terminated) {
      memset(rcommand, '\0', MAX_CMD_LEN);
      n = 0;
    }
  }
  limitCharWait(rcommand, n);
}


bool SerialInterface::parseCmd(char* rcommand, bool terminated)
{
  String scommand(rcommand);
  scommand.trim();
  scommand.toLowerCase();
  bool success =
    workWithModeSwitch(scommand, terminated) ||
    workWithStatus(scommand, terminated) ||
    workWithThermometers(scommand, terminated) ||
    workWithConfigParams(scommand, terminated) ||
    workWithTempControl(scommand, terminated) ||
    workWithControlOutput(scommand, terminated);
  if (!success && scommand.length() > 0 && terminated) {
    Serial.print("Unable to parse command: ");
    Serial.println(scommand);
  }
  return success;
}


bool SerialInterface::workWithModeSwitch(const String &scommand, bool terminated) {
  return
    workWithCommand("heat-mode", &heatMode, scommand, terminated) ||
    workWithCommand("standby", &standby, scommand, terminated) ||
    workWithCommand("config", &config, scommand, terminated) ||
    workWithCommand("save-config", &saveConfig, scommand, terminated) ||
    workWithCommand("discard-config", &discardConfig, scommand, terminated);
}


bool SerialInterface::workWithStatus(const String &scommand, bool terminated) {
  return
    workWithCommand("Status?", &printStatus, scommand, terminated) ||
      workWithQuery<HeatSysMode>("Status.Mode", Status::mode, &printMode, scommand, terminated) ||
      workWithQuery<ZoneStatus>("Status.FloorHeatZone", Status::floorHeatRoom, &printZoneStatus, scommand, terminated) ||
        workWith<float>("Status.FloorHeatZone.Setpoint", Status::floorHeatZoneSetpoint, &parseFloat, &printFloat, scommand, terminated) ||
        workWithQuery<float>("Status.FloorHeatZone.DisplayValue", Status::floorHeatRoom.displayValue, &printFloat, scommand, terminated) ||
        workWithQuery<bool>("Status.FloorHeatZone.Estimated", Status::floorHeatRoom.estimated, &printBool, scommand, terminated) ||
        workWithQuery<bool>("Status.FloorHeatZone.HeatNeeded", Status::floorHeatRoom.heatingNecessary, &printBool, scommand, terminated) ||
      workWithQuery<ZoneStatus>("Status.RadiatorZone", Status::radiatorRoom, &printZoneStatus, scommand, terminated) ||
        workWith<float>("Status.RadiatorZone.Setpoint", Status::radiatorZoneSetpoint, &parseFloat, &printFloat, scommand, terminated) ||
        workWithQuery<float>("Status.RadiatorZone.DisplayValue", Status::radiatorRoom.displayValue, &printFloat, scommand, terminated) ||
        workWithQuery<bool>("Status.RadiatorZone.Estimated", Status::radiatorRoom.estimated, &printBool, scommand, terminated) ||
        workWithQuery<bool>("Status.RadiatorZone.HeatNeeded", Status::radiatorRoom.heatingNecessary, &printBool, scommand, terminated) ||
      workWithQuery<ErrorCode>("Status.Alarm", Status::errCode, &printError, scommand, terminated);
}


bool SerialInterface::workWithThermometers(const String &scommand, bool terminated) {
  return
    workWithCommand("Thermometers?", &printThermometers, scommand, terminated) ||
      workWithQuery<AbstractThermometer>("Thermometers.RadiatorZone", Thermometers::radiatorRoom, &printThermometer, scommand, terminated) ||
      workWithQuery<AbstractThermometer>("Thermometers.FloorHeatZone", Thermometers::floorHeatRoom, &printThermometer, scommand, terminated) ||
      workWithQuery<AbstractThermometer>("Thermometers.WoodBoiler", Thermometers::woodBoiler, &printThermometer, scommand, terminated) ||
      workWithQuery<AbstractThermometer>("Thermometers.BufferWater", Thermometers::bufferWater, &printThermometer, scommand, terminated);
}


bool SerialInterface::workWithConfigParams(const String &scommand, bool terminated) {
  if (Status::mode == HeatSysMode::MAINTAINANCE) return
   workWithCommand("ConfigParams?", &printConfigParams, scommand, terminated) ||
      workWithQuery<float>("ConfigParams.FreezeAlarmTemp", ConfigParams::FREEZE_ALARM_TEMP, &printFloat, scommand, terminated) ||
      workWithQuery<float>("ConfigParams.FreezeAlarmRstTemp", ConfigParams::FREEZE_ALARM_DISA_TEMP, &printFloat, scommand, terminated) ||
      workWithQuery<float>("ConfigParams.OverheatAlarmTemp", ConfigParams::OVERHEAT_ALARM_TEMP, &printFloat, scommand, terminated) ||
      workWithQuery<float>("ConfigParams.OverheatAlarmRstTemp", ConfigParams::OVERHEAT_LOCK_OPEN_TEMP, &printFloat, scommand, terminated) ||
      workWith<float>("ConfigParams.DelthaTemp", ConfigParams::delthaTemp, &parseFloat, &printFloat, scommand, terminated) ||
      workWith<float>("ConfigParams.MinBufWaterTemp", ConfigParams::minBufWaterTemp, &parseFloat, &printFloat, scommand, terminated);
  else return
    workWithCommand("ConfigParams?", &printConfigParams, scommand, terminated) ||
      workWithQuery<float>("ConfigParams.FreezeAlarmTemp", ConfigParams::FREEZE_ALARM_TEMP, &printFloat, scommand, terminated) ||
      workWithQuery<float>("ConfigParams.FreezeAlarmRstTemp", ConfigParams::FREEZE_ALARM_DISA_TEMP, &printFloat, scommand, terminated) ||
      workWithQuery<float>("ConfigParams.OverheatAlarmTemp", ConfigParams::OVERHEAT_ALARM_TEMP, &printFloat, scommand, terminated) ||
      workWithQuery<float>("ConfigParams.OverheatAlarmRstTemp", ConfigParams::OVERHEAT_LOCK_OPEN_TEMP, &printFloat, scommand, terminated) ||
      workWithQuery<float>("ConfigParams.DelthaTemp", ConfigParams::delthaTemp, &printFloat, scommand, terminated) ||
      workWithQuery<float>("ConfigParams.MinBufWaterTemp", ConfigParams::minBufWaterTemp, &printFloat, scommand, terminated);

}


bool SerialInterface::workWithTempControl(const String &scommand, bool terminated) {
  return
    workWithCommand("HeatControl?", &printTempControl, scommand, terminated) ||
      workWithQuery<ZoneStatus>("HeatControl.FloorHeatZoneStatus", TempControl::floorHeatRoomStatus, &printZoneStatus, scommand, terminated) ||
        workWithQuery<float>("HeatControl.FloorHeatZoneStatus.Setpoint", TempControl::floorHeatRoomStatus.setpoint, &printFloat, scommand, terminated) ||
        workWithQuery<float>("HeatControl.FloorHeatZoneStatus.DisplayValue", TempControl::floorHeatRoomStatus.displayValue, &printFloat, scommand, terminated) ||
        workWithQuery<bool>("HeatControl.FloorHeatZoneStatus.Estimated", TempControl::floorHeatRoomStatus.estimated, &printBool, scommand, terminated) ||
        workWithQuery<bool>("HeatControl.FloorHeatZoneStatus.HeatNeeded", TempControl::floorHeatRoomStatus.heatingNecessary, &printBool, scommand, terminated) ||
      workWithQuery<ZoneStatus>("HeatControl.RadiatorZoneStatus", TempControl::radiatorRoomStatus, &printZoneStatus, scommand, terminated) ||
        workWithQuery<float>("HeatControl.RadiatorZoneStatus.Setpoint", TempControl::radiatorRoomStatus.setpoint, &printFloat, scommand, terminated) ||
        workWithQuery<float>("HeatControl.RadiatorZoneStatus.DisplayValue", TempControl::radiatorRoomStatus.displayValue, &printFloat, scommand, terminated) ||
        workWithQuery<bool>("HeatControl.RadiatorZoneStatus.Estimated", TempControl::radiatorRoomStatus.estimated, &printBool, scommand, terminated) ||
        workWithQuery<bool>("HeatControl.RadiatorZoneStatus.HeatNeeded", TempControl::radiatorRoomStatus.heatingNecessary, &printBool, scommand, terminated) ||
      workWithQuery<bool>("HeatControl.NormalMode", TempControl::normalMode, &printBool, scommand, terminated) ||
      workWithQuery<bool>("HeatControl.EstimationMode", TempControl::estimationMode, &printBool, scommand, terminated) ||
      workWithQuery<bool>("HeatControl.SingleZoneMode", TempControl::oneRoomTemperatureOnlyMode, &printBool, scommand, terminated) ||
      workWithQuery<bool>("HeatControl.HeatDecisionFailure", TempControl::heatNecessaryDecisionError, &printBool, scommand, terminated) ||
      workWithQuery<bool>("HeatControl.WoodPumpDecisionFailure", TempControl::woodBoilerPumpDecisionError, &printBool, scommand, terminated) ||
      workWithQuery<bool>("HeatControl.WoodGasDecisionFailure", TempControl::bufferOrGasBoilerDecisionError, &printBool, scommand, terminated) ||
      workWithQuery<bool>("HeatControl.FreezeAlarm", TempControl::freezeEmergency, &printBool, scommand, terminated) ||
      workWithQuery<bool>("HeatControl.OverheatAlarm", TempControl::overheatEmergency, &printBool, scommand, terminated);
}


bool SerialInterface::workWithControlOutput(const String &scommand, bool terminated) {
  if (Status::mode == HeatSysMode::MAINTAINANCE) return
    workWithCommand("ControlOutput?", &printControlOutput, scommand, terminated) ||
      workWith<bool>("ControlOutput.FloorHeatPump", ControlOutput::floorHeatPump, &parseBool, &printBool, scommand, terminated) ||
      workWith<bool>("ControlOutput.WoodBoilerPump", ControlOutput::woodBoilerPump, &parseBool, &printBool, scommand, terminated) ||
      workWith<bool>("ControlOutput.BufferPump", ControlOutput::bufferPump, &parseBool, &printBool, scommand, terminated) ||
      workWith<bool>("ControlOutput.GasBoiler", ControlOutput::gasBoiler, &parseBool, &printBool, scommand, terminated);
  else return
    workWithCommand("ControlOutput?", &printControlOutput, scommand, terminated);
      workWithQuery<bool>("ControlOutput.FloorHeatPump", ControlOutput::floorHeatPump, &printBool, scommand, terminated) ||
      workWithQuery<bool>("ControlOutput.WoodBoilerPump", ControlOutput::woodBoilerPump, &printBool, scommand, terminated) ||
      workWithQuery<bool>("ControlOutput.BufferPump", ControlOutput::bufferPump, &printBool, scommand, terminated) ||
      workWithQuery<bool>("ControlOutput.GasBoiler", ControlOutput::gasBoiler, &printBool, scommand, terminated);
}


bool SerialInterface::workWithCommand(const String &name, void (*cmdFun)(), const String &scommand, bool terminated) {
  if (scommand.equalsIgnoreCase(name)) {
    cmdFun();
    return true;
  }
  return false;
}


template<typename T> bool SerialInterface::workWithQuery(const String &name, const T &variable, void (*print)(const String &prefix, const String &name, const T &value), const String &scommand, bool terminated) {
  String token = name + "?";
  token.toLowerCase();
  if (scommand.startsWith(token)) {
    print("", name, variable);
    return true;
  }
  return false;
}


template <typename T> bool SerialInterface::workWith(const String &name, T &variable, bool (*parse)(const String &sval, T &variable, bool terminated), void (*print)(const String &prefix, const String &name, const T &value), const String &scommand, bool terminated) {
  if (workWithQuery<T>(name, variable, print, scommand, terminated))
    return true;
  String token = name + "!";
  token.toLowerCase();
  if (scommand.startsWith(token)) {
    String sval = scommand.substring(token.length());
    sval.trim();
    if (parse(sval, variable, terminated)) {
      print("", name, variable);
      return true;
    }
  }
  return false;
}


bool SerialInterface::parseFloat(const String &sval, float &variable, bool terminated) {
  float value = sval.toFloat();
  if (value != NAN) {
    variable = value;
    return true;
  }
  if (terminated) {
    Serial.print("Unable to parse as float: ");
    Serial.println(sval);
  }
  return false;
}


bool SerialInterface::parseBool(const String &sval, bool &variable, bool terminated) {
  if (sval.equals("y") || sval.equals("yes") || sval.equals("true")) {
    variable = true;
    return true;
  }
  if (sval.equals("n") || sval.equals("no") || sval.equals("false")) {
    variable = false;
    return true;
  }
  if (terminated) {
    Serial.print("Unable to parse as bool: ");
    Serial.println(sval);
  }
  return false;
}


void SerialInterface::printFloat(const String &prefix, const String &name, const float &value) {
  Serial.print(prefix + name + ": ");
  Serial.println(value);
}


void SerialInterface::printBool(const String &prefix, const String &name, const bool &value) {
  Serial.print(prefix + name);
  Serial.println(value ? ": YES" : ": NO");
}


void SerialInterface::printMode(const String &prefix, const String &name, const HeatSysMode &value) {
  Serial.print(prefix + name);
  Serial.println(
    value == HeatSysMode::HEATING ? ": HEATING" :
    value == HeatSysMode::MAINTAINANCE ? ": CONFIG" :
    ": STANDBY"
  );
}


void SerialInterface::printError(const String &prefix, const String &name, const ErrorCode &value) {
  Serial.print(prefix + name + ": 0x"); Serial.println(Status::errCode, HEX);
  if (value & ErrorCode::UNSAT_ESTIMATION)
    Serial.println(prefix + "\t0x0001\tUnsatisfying estimation");  // for future version
  if (value & ErrorCode::MASTER_LOST_TMOUT)
    Serial.println(prefix + "\t0x0002\tSerial master lost");  // no sense on serial interface
  if (value & ErrorCode::RAD_ROOM_TEMP_SENS_ERR)
    Serial.println(prefix + "\t0x0004\tRadiator-room thermometer failure");
  if (value & ErrorCode::FLOOR_ROOM_TEMP_SENS_ERR)
    Serial.println(prefix + "\t0x0008\tFloor-heat room thermometer failure");
  if (value & ErrorCode::WOOD_BOIL_TEMP_SENS_ERR)
    Serial.println(prefix + "\t0x0010\tWood boiler thermometer failure");
  if (value & ErrorCode::BUF_TEMP_SENS_ERR)
    Serial.println(prefix + "\t0x0020\tBuffer thermometer failure");
  if (value & ErrorCode::ESTIMATION_MODE)
    Serial.println(prefix + "\t0x0040\tEstimation mode active");
  if (value & ErrorCode::SINGLE_TEMP_MEAS_MODE)
    Serial.println(prefix + "\t0x0080\t1-zone mode");
  if (value & ErrorCode::WOOD_BOIL_PUMP_DEC_WARN)
    Serial.println(prefix + "\t0x0100\tWood-boiler pump decision failure");
  if (value & ErrorCode::BUF_OR_GAS_BOIL_DEC_ERR)
    Serial.println(prefix + "\t0x0200\tBuffer-or-gas decision failure");
  if (value & ErrorCode::HEAT_NEC_DEC_ERR)
    Serial.println(prefix + "\t0x0400\tHeat-decision failure");
  if (value & ErrorCode::UNSET_DEVICE_ADR_ERR)
    Serial.println(prefix + "\t0x0800\tUnset or invalid 1-wire bus-address");
  if (value & ErrorCode::FREEZE_EMERGENCY)
    Serial.println(prefix + "\t0x1000\tFreeze-alarm");
  if (value & ErrorCode::OVERHEAT_EMERGENCY)
    Serial.println(prefix + "\t0x2000\tOverheat-alarm");
}


void SerialInterface::printZoneStatus(const String &prefix, const String &name, const ZoneStatus &value) {
  Serial.println(prefix + name + ":");
  printFloat(prefix, "\tSetpoint", value.setpoint);
  printFloat(prefix, "\tDisplayValue", value.displayValue);
  printBool(prefix, "\tEstimated", value.estimated);
  printBool(prefix, "\tHeatNeeded", value.heatingNecessary);
}


void SerialInterface::printThermometer(const String &prefix, const String &name, const AbstractThermometer &thermometer) {
  Serial.print(prefix + name + ": ");
  Serial.println(thermometer.temperature);
  printBool(prefix + "\t", "Error", thermometer.error);
}


void SerialInterface::printStatus() {
  Serial.println("Status:");
  printMode("\t", "Mode", Status::mode);
  printZoneStatus("\t","RadiatorZone", Status::radiatorRoom);
  printZoneStatus("\t","FloorHeatZone", Status::floorHeatRoom);
  printError("\t", "Alarm", Status::errCode);
}


void SerialInterface::printThermometers() {
  Serial.println("Thermometers:");
  printThermometer("\t", "RadiatorZone", Thermometers::radiatorRoom);
  printThermometer("\t", "FloorHeatZone", Thermometers::floorHeatRoom);
  printThermometer("\t", "WoodBoiler", Thermometers::woodBoiler);
  printThermometer("\t", "BufferWater", Thermometers::bufferWater);
}


void SerialInterface::printConfigParams() {
  Serial.println("ConfigParams:");
  printFloat("\t", "FREEZE_ALARM_TEMP", ConfigParams::FREEZE_ALARM_TEMP);
  printFloat("\t", "FREEZE_ALARM_RST_TEMP", ConfigParams::FREEZE_ALARM_DISA_TEMP);
  printFloat("\t", "OVERHEAT_ALARM_TEMP", ConfigParams::OVERHEAT_ALARM_TEMP);
  printFloat("\t", "OVERHEAT_ALARM_RST_TEMP", ConfigParams::OVERHEAT_LOCK_OPEN_TEMP);
  printFloat("\t", "DelthaTemp", ConfigParams::delthaTemp);
  printFloat("\t", "MinBufWaterTemp", ConfigParams::minBufWaterTemp);
}


void SerialInterface::printTempControl() {
  Serial.println("HeatControl:");
  printZoneStatus("\t", "RadiatorZoneStatus", TempControl::radiatorRoomStatus);
  printZoneStatus("\t", "FloorHeatZoneStatus", TempControl::floorHeatRoomStatus);
  printBool("\t", "NormalMode", TempControl::normalMode);
  printBool("\t", "EstimationMode", TempControl::estimationMode);
  printBool("\t", "SingleZoneMode", TempControl::oneRoomTemperatureOnlyMode);
  printBool("\t", "HeatDecisionFailure", TempControl::heatNecessaryDecisionError);
  printBool("\t", "WoodPumpDecisionFailure", TempControl::woodBoilerPumpDecisionError);
  printBool("\t", "WoodGasDecisionFailure", TempControl::bufferOrGasBoilerDecisionError);
  printBool("\t", "FreezeAlarm", TempControl::freezeEmergency);
  printBool("\t", "OverheatAlarm", TempControl::overheatEmergency);
}


void SerialInterface::printControlOutput() {
  Serial.println("ControlOutput:");
  printBool("\t", "FloorHeatPump", ControlOutput::floorHeatPump);
  printBool("\t", "WoodBoilerPump", ControlOutput::woodBoilerPump);
  printBool("\t", "BufferPump", ControlOutput::bufferPump);
  printBool("\t", "GasBoiler", ControlOutput::gasBoiler);
}


void SerialInterface::limitCharWait(char* rcommand, size_t& n)
{
  static unsigned long last = 0;
  static unsigned long tick = 0;
  static unsigned long tock = 0;
  tock = millis();
  tick = (n == last) ? tock : tick;
  if (tock - tick >= MAX_MILLIS_WAIT_CHAR) {
    memset(rcommand, '\0', MAX_CMD_LEN);
    n = 0;
    last = 0;
  }
}
